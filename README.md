## General info

Basic Features (API endpoints):
1. Catalog actions: create, get list.
2. Course actions: create, see detail and list of courses, edit, delete, search, filter.

You can find site [here](https://courses-yal.herokuapp.com)

Used technology: Django, Django Rest Framework, Swagger, Heroku, Environ.

## Start project
```
python3 -m venv or virtualenv -p python3 venv
for linux source venv/bin/activate
for windows venv\Scripts\activate
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver
```