from .settings import *

import dj_database_url
import django_heroku


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.getenv('DB_NAME') or env('DB_NAME', default='y_db'),
        'USER': os.getenv('DB_USER') or env('DB_USER', default='y_admin'),
        'PASSWORD': os.getenv('DB_PASSWORD') or env('DB_PASSWORD', default='y_password'),
        'HOST': os.getenv('DB_HOST') or env('DB_HOST', default='localhost'),
        'PORT': int(os.getenv('DB_PORT') or env('DB_PORT', default=5432)),
    }
}

db_from_env = dj_database_url.config(conn_max_age=500)
DATABASES['default'].update(db_from_env)

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Europe/Kiev'

DATETIME_FORMAT = '%d-%m-%Y %H:%m'
DATE_FORMAT = '%d-%m-%Y'

django_heroku.settings(locals())
