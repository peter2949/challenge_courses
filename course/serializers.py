from rest_framework import serializers

from config.settings import DATE_FORMAT
from course.models import Catalog, Course


class CatalogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Catalog
        fields = ('name',)


class CourseSerializer(serializers.ModelSerializer):
    date_start = serializers.DateField(format=DATE_FORMAT)
    date_end = serializers.DateField(format=DATE_FORMAT)

    class Meta:
        model = Course
        fields = '__all__'
