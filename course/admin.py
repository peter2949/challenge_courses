from django.contrib import admin

from course.models import Catalog, Course


admin.site.register(Catalog)
admin.site.register(Course)
