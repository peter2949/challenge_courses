from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from course.views import CourseViewSet, CatalogAPIView

urlpatterns = [
    path('', CourseViewSet.as_view({'get': 'list'}), name='list'),
    path('course/<int:pk>/', CourseViewSet.as_view({'get': 'retrieve'}), name='detail'),
    path('course/create/', CourseViewSet.as_view({'post': 'create'}), name='create'),
    path('course/delete/<int:pk>/', CourseViewSet.as_view({'delete': 'destroy'}), name='delete'),
    path('course/update/<int:pk>/', CourseViewSet.as_view({'put': 'update'}), name='update'),
    path('catalog/create/', CatalogAPIView.as_view(), name='create_catalog'),
    path('catalog/list/', CatalogAPIView.as_view(), name='catalog_list')
]
