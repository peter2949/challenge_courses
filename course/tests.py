from django.test import TestCase
from rest_framework import status
from rest_framework.reverse import reverse

from course.models import Catalog, Course


class CatalogModelTest(TestCase):
    def setUp(self):
        Catalog.objects.create(name='English')

    def test_catalog_name(self):
        cat = Catalog.objects.get(id=1)
        expected_object_name = f'{cat.name}'
        self.assertEquals(expected_object_name, 'English')

    def test_create_catalog(self):
        data = {
            "name": "Python",
        }
        url = reverse('create_catalog')
        response = self.client.post(url, data)
        self.assertEqual(Catalog.objects.count(), 2)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class CourseModelTest(TestCase):
    def setUp(self):
        Catalog.objects.create(name='English')
        Course.objects.create(
            catalog=1, name='Course English Beginner', date_start='16-01-2021', date_end='11-03-2021',
            quantity_lectures=10
        )

    def test_create_course(self):
        data = {
            "name": "Course English Advanced",
            "date_start": "2021-06-11",
            "date_end": "2021-09-01",
            "quantity_lectures": 5,
            "catalog": 1
        }
        url = reverse('create')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Course.objects.count(), 1)
        self.assertEqual(Course.objects.get().quantity_lectures, 5)

    def test_update_data(self):
        data = {
            "name": "Course English Advanced",
            "date_start": "2021-06-11",
            "date_end": "2021-09-01",
            "quantity_lectures": 11,
            "catalog": 1
        }
        url = reverse('update', kwargs={'pk': 0})
        response = self.client.put(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_data(self):
        url = reverse('delete', kwargs={'pk': 1})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
