from django.db import models


class Catalog(models.Model):
    name = models.CharField('name', max_length=128, unique=True)


class Course(models.Model):
    """ Contains the data about course """
    catalog = models.ForeignKey(Catalog, verbose_name='Catalog', on_delete=models.CASCADE, related_name='course')
    name = models.CharField('name', max_length=128)
    date_start = models.DateField('date beginning')
    date_end = models.DateField('date end')
    quantity_lectures = models.PositiveSmallIntegerField('quantity lectures')
