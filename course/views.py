from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, filters
from rest_framework.generics import ListCreateAPIView
from rest_framework.permissions import AllowAny

from course.models import Catalog, Course
from course.serializers import CourseSerializer, CatalogSerializer


class CatalogAPIView(ListCreateAPIView):
    queryset = Catalog.objects.all()
    serializer_class = CatalogSerializer
    permission_classes = [AllowAny, ]


class CourseViewSet(viewsets.ModelViewSet):
    """ provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions for course """

    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    search_fields = ['name', ]
    filterset_fields = ('date_start', 'date_end')
    permission_classes = [AllowAny, ]
